<?= $this->extend('templates/admin_template') ?>

<?= $this->section('content') ?>

    <div class="container-fluid pt-2">
        <div class="row">
            <div class="col-md-12">
            <div class="card card-dark">
              <div class="card-header">
                <h3 class="card-title text-sm">Create Activity and Budget</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form">
                <div class="card-body">
                  <div class="row">
                    <div class="form-group col-md-2">
                        <label for="exampleInputEmail1">Select Budget Year:</label>
                        <select class="form-control form-control-sm">
                            <option></option>
                        </select>
                    </div>
                    <div class="form-group col-md-10">
                        <label for="exampleInputEmail1">National Sector:</label>
                        <select class="form-control form-control-sm">
                            <option></option>
                        </select>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">National Policy:</label>
                        <select class="form-control form-control-sm">
                            <option></option>
                        </select>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-md-10">
                        <label for="exampleInputEmail1">Local Sector:</label>
                        <select class="form-control form-control-sm">
                            <option></option>
                        </select>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="exampleInputEmail1">Sector Total:</label>
                        <input class="form-control form-control-sm" type="text" placeholder="">
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-md-10">
                        <label for="exampleInputEmail1">Local Policy:</label>
                        <select class="form-control form-control-sm">
                            <option></option>
                        </select>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="exampleInputEmail1">Policy Total:</label>
                        <input class="form-control form-control-sm" type="text" placeholder="">
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-md-10">
                        <label for="exampleInputEmail1">Strategy:</label>
                        <select class="form-control form-control-sm">
                            <option></option>
                        </select>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="exampleInputEmail1">Strategy Total:</label>
                        <input class="form-control form-control-sm" type="text" placeholder="">
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-md-10">
                        <label for="exampleInputEmail1">Activity:</label>
                        <select class="form-control form-control-sm">
                            <option></option>
                        </select>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="exampleInputEmail1">Activity Total:</label>
                        <input class="form-control form-control-sm" type="text" placeholder="">
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-md-2">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                <label class="form-check-label" for="inlineCheckbox1">New Activity</label>
                            </div>
                        </div>
                        <div class="form-group col-md-10">
                            <label for="exampleInputEmail1">New Activity in English</label>
                            <input class="form-control form-control-sm" type="text" placeholder="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-2">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                <label class="form-check-label" for="inlineCheckbox1">PSIP</label>
                            </div>
                        </div>
                        <div class="form-group col-md-10">
                            <label for="exampleInputEmail1">New Activity in Dhivehi</label>
                            <input class="form-control form-control-sm" type="text" placeholder="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-2">
                            <label for="exampleInputEmail1">Indicator:</label>
                            <input class="form-control form-control-sm" type="text" placeholder="">
                        </div>
                        <div class="form-group col-md-2">
                            <label for="exampleInputEmail1">Year 2020 Target:</label>
                            <input class="form-control form-control-sm" type="text" placeholder="">
                        </div>
                        <div class="form-group col-md-2">
                            <label for="exampleInputEmail1">Year 2021 Target:</label>
                            <input class="form-control form-control-sm" type="text" placeholder="">
                        </div>
                        <div class="form-group col-md-2">
                            <label for="exampleInputEmail1">Year 2022 Target:</label>
                            <input class="form-control form-control-sm" type="text" placeholder="">
                        </div>        
                  </div>

                  <div class="row">
                        <div class="form-group col-md-2">
                            <label for="exampleInputEmail1">Fund:</label>
                            <select class="form-control form-control-sm">
                                <option></option>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="exampleInputEmail1">Code Detail:</label>
                            <input class="form-control form-control-sm" type="text" placeholder="">
                        </div>
                        <div class="form-group col-md-2">
                            <label for="exampleInputEmail1">Code Allocation:</label>
                            <input class="form-control form-control-sm" type="text" placeholder="">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="exampleInputEmail1">Business Center:</label>
                            <input class="form-control form-control-sm" type="text" placeholder="">
                        </div>        
                  </div>

                  <div class="row">
                        <div class="form-group col-md-2">
                            <label for="exampleInputEmail1">GL Code:</label>
                            <select class="form-control form-control-sm">
                                <option></option>
                            </select>
                        </div>
                        <div class="form-group col-md-8">
                            <label for="exampleInputEmail1">Code Detail:</label>
                            <input class="form-control form-control-sm" type="text" placeholder="">
                        </div>
                        <div class="form-group col-md-2">
                            <label for="exampleInputEmail1">Priority:</label>
                            <input class="form-control form-control-sm" type="text" placeholder="">
                        </div>        
                  </div>

                  <div class="row">
                        <div class="form-group col-md-2">
                            <label for="exampleInputEmail1">GL Code Amount:</label>
                            <select class="form-control form-control-sm">
                                <option></option>
                            </select>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="exampleInputEmail1">Year 2021 Forcast:</label>
                            <input class="form-control form-control-sm" type="text" placeholder="">
                        </div>
                        <div class="form-group col-md-2">
                            <label for="exampleInputEmail1">Year 2022 Forcast:</label>
                            <input class="form-control form-control-sm" type="text" placeholder="">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="exampleInputEmail1">Start Date:</label>
                            <input class="form-control form-control-sm" type="text" placeholder="">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="exampleInputEmail1">End Date:</label>
                            <input class="form-control form-control-sm" type="text" placeholder="">
                        </div>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-secondary btn-sm">Activity Update</button>
                  <button type="submit" class="btn btn-secondary btn-sm">Save</button>
                  <button type="submit" class="btn btn-secondary btn-sm">Delete</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
        </div>
    </div>

<?= $this->endSection() ?>