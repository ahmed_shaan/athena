<?= $this->extend('templates/admin_template') ?>

<?= $this->section('content') ?>

<div class="container-fluid pt-2">
        <div class="row">
            <div class="col-md-12">
            <div class="card card-dark">
              <div class="card-header">
                <h3 class="card-title text-sm">Manage Business Centers</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form">
                <div class="card-body">
                  <div class="row">
                    <div class="form-group col-md-4">
                        <label for="exampleInputEmail1">Center Name:</label>
                        <input class="form-control form-control-sm" type="text" placeholder="">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="exampleInputEmail1">Center in Dhivehi:</label>
                        <input class="form-control form-control-sm" type="text" placeholder="">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="exampleInputEmail1">Center Detail:</label>
                        <input class="form-control form-control-sm" type="text" placeholder="">
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-md-4">
                        <label for="exampleInputEmail1">Location</label>
                        <input class="form-control form-control-sm" type="text" placeholder="">
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-secondary btn-sm">Clear</button>
                  <button type="submit" class="btn btn-secondary btn-sm">Update</button>
                  <button type="submit" class="btn btn-secondary btn-sm">Save</button>
                  <button type="submit" class="btn btn-secondary btn-sm">Import</button>
                </div>
              </form>
            </div>
            <div class="card card-dark">
              <div class="card-header">
                <h3 class="card-title text-sm">Business Centers</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th></th>
                      <th style="width: 5px">#</th>
                      <th>Location</th>
                      <th>Center Name</th>
                      <th>Dhivehi Name</th>
                      <th>Center No</th>
                      <th>Detail</th>
                      <th>Created Date</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Select</td>
                      <td>1</td>
                      <td>N.Landhoo</td>
                      <td>Idhaaree Hingun</td>
                      <td>އިދާރީހިންގުން</td>
                      <td>2</td>
                      <td>Idhaaree Hingun</td>
                      <td>2/19/2020 8:31:00 AM</td>
                    </tr>
                    <tr>
                      <td>Select</td>
                      <td>2</td>
                      <td>N.Landhoo</td>
                      <td>Rahvehi Hidhumai</td>
                      <td>ރައްވެހި ހިދުމަތް</td>
                      <td>1</td>
                      <td>Rahvehi Hidhumai</td>
                      <td>2/18/2020 8:31:00 AM</td>
                    </tr>                  
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
              <div class="row">
                  <div class="form-group">
                    <p class="pl-3 pt-4"><strong>Current Budget Year</strong>: 2020 | System Running Mode: Voucher Mode | Version: 2.5</p>
                  </div>
              </div>
            </div>
        </div>
    </div>

<?= $this->endSection() ?>