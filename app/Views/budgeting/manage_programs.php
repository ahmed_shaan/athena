<?= $this->extend('templates/admin_template') ?>

<?= $this->section('content') ?>

<div class="container-fluid pt-2">
        <div class="row">
            <div class="col-md-12">
            <div class="card card-dark">
              <div class="card-header">
                <h3 class="card-title text-sm">Manage Programs</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form">
                <div class="card-body">
                  <div class="row">
                    <div class="form-group col-md-2">
                        <label for="exampleInputEmail1">Select Budget Year:</label>
                        <select class="form-control form-control-sm">
                            <option></option>
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="exampleInputEmail1">Data Type:</label>
                        <select class="form-control form-control-sm">
                            <option></option>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1">Main Category/Local Policies:</label>
                        <select class="form-control form-control-sm">
                            <option></option>
                        </select>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1">Detail (Eng)</label>
                        <input class="form-control form-control-sm" type="text" placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1">Detail (Dhivehi)</label>
                        <input class="form-control form-control-sm" type="text" placeholder="">
                    </div>
                    <div class="row">
                      <div class="form-group">
                        <p class="pl-3 pt-4">Government Manifesto and Policy has to be created for each year.</p>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-secondary btn-sm">Clear</button>
                  <button type="submit" class="btn btn-secondary btn-sm">Update</button>
                  <button type="submit" class="btn btn-secondary btn-sm">Save</button>
                  <button type="submit" class="btn btn-secondary btn-sm">Import</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
              <div class="row">
                  <div class="form-group">
                    <p class="pl-3 pt-4"><strong>Current Budget Year</strong>: 2020 | System Running Mode: Voucher Mode | Version: 2.5</p>
                  </div>
              </div>
            </div>
        </div>
    </div>

<?= $this->endSection() ?>