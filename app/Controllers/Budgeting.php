<?php namespace App\Controllers;

class Budgeting extends BaseController
{
	public function expense_activities()
	{
		return view('budgeting/expense_activities');
	}

	public function manage_business_centers()
	{
		return view('budgeting/manage_business_centers');
	}

	public function manage_programs()
	{
		return view('budgeting/manage_programs');
	}

	

}